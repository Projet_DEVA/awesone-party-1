#include "miniJeu.h"

int *lancerMiniJeu(char **joueurs, int nbJoueurs, int difficulty)
{
  int choix, i, j, index, scorebuffer = -1, *classement = (int *)malloc(sizeof(int) * 4);
  choix = rand() % 3;
  int *score;
  printChat("C'est l'heure du mini-jeu !\n");
  switch (choix)
  {
  case 0:
    printChat("Dans ce mini-jeu, vous devrez appuyer le plus de fois sur les touches qui vont vous êtres indiques !");
    printChat("Appuyez sur une touche pour lancer le mini-jeu");
    getch();
    clearAffichageChat();
    score = spamMe(nbJoueurs, joueurs, difficulty);
    break;
  case 1:
    printChat("Dans ce mini-jeu, vous devrez essayer de devenier un nombre\n");
    printChat("A chaque essaie, on vous indiquera si la reponse est superieur ou inferieur a votre proposition !\n");
    printChat("Appuyez sur une touche pour lancer le mini-jeu");
    getch();
    clearAffichageChat();
    score = plusMoins(nbJoueurs, joueurs, difficulty);
    break;
  case 2:
    printChat("Dans ce mini-jeu, vous devrez prendre le plus de pieces au dragon\n");
    printChat("Mais attention a ne pas le reveiller ! Si vous le reveillez, vous perdez toutes vos pieces !\n");
    printChat("Vous pouvez prendre une a trois pieces au dragon par tour\n");
    printChat("Appuyez sur une touche pour lancer le mini-jeu");
    getch();
    clearAffichageChat();
    score = dragon(nbJoueurs, joueurs, difficulty);
  }
  for (j = 0; j < 4; j++)
  {
    for (i = 0; i < 4; i++)
    {
      if (score[i] > scorebuffer)
      {
        scorebuffer = score[i];
        index = i;
      }
    }
    classement[j] = index;
    score[index] = -1;
    scorebuffer = -1;
  }

  free(score);
  return classement;
}
