#ifndef spamMe_h
#define spamMe_h

#include <time.h>
#include <stdlib.h>
#include <curses.h>

int *spamMe(int nbJoueurs, char **joueurs, int difficulty);

extern void printChat(const char *string, ...);
extern int scanChat(const char *string, ...);
extern void clearAffichageChat();
#endif
