#include "affichage.h"
#include "event.h"
#include "objet.h"

int main()
{
  int choix = -1;
  while (1)
  {
    do
    {
      choix = menuPrincipal();
    } while (choix == -1);
    if(choix == 0){
      tutoriel();
    }
    else if (choix == 1)
    {
      int i, j, nbtours, selection;
      char pl[50];
      initscr();
      def_couleurs();
      if ((selection = selectionPlateau()) != -1)
      {
        sprintf(pl, "map/map%d.txt", selection);
        Plateau a = importBoard(pl);
        choixModeDeJeux(&a);
        initEtat();
        initChat();
        printChat("\n");
        while (a.difficulty > 3 || a.difficulty <= 0)
        {
          printChat("%s", "Entrez la difficulte voulu (1-3): ");
          scanChat("%d", &a.difficulty);
        }
        do
        {
          printChat("%s", "Entrez le nombre de tour : ");
        } while (scanChat("%d", &nbtours) != 1);
        clearAffichageChat();
        def_couleurs();

        for (j = 0; j < nbtours; j++)
        {
          turn(&a, j + 1);
          if (j % 6 == 5)
          {
            miniJeux(a.joueurs, a.nbJoueurs, a.difficulty);
          }
          if (j % 7 == 6)
          {
            startEvent(&a);
          }
        }
        clearAffichageChat();
        printChat("Fin : \n");
        for (i = 0; i < 4; i++)
        {
          printChat("Le joueur %d du nom de %s est a la case %d avec %d pieces\n", a.joueurs[i].numero, a.joueurs[i].nom, a.joueurs[i].position->indiceTab, a.joueurs[i].argent);
          refresh();
        }
        sauvegarde(&a);
        supprimerPlateau(&a);
        delChat();
        delEtat();
        erase();
        endwin();
        afficherSauvegarde(1);
      }
      else
      {
        erase();
        endwin();
      }
    }
    else if (choix == 2)
    {
      afficherSauvegarde(0);
    }
    else if (choix == 3)
    {
      return EXIT_SUCCESS;
    }
  }
}
