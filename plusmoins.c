#include "plusmoins.h"

int *plusMoins(int nbJoueurs, char** joueurs, int difficulty)
{
int rep=0;
int count=0;
int t1;  //recupera le temps restant si la reponse est trouve
int *score=(int*)malloc(4*sizeof(int));
for(int i = 0 ; i < nbJoueurs ; i++ )
{
  printChat("Joueur %s c'est à vous de jouer :\n",joueurs[i]);
  int j=rand()%(10000-1)+1;   //avec rand premier nombre = 42
  printChat("Un nombre a ete genere vous avez donc 45 secondes pour le deviner.\n");
  printChat("Ca commence !\n");
  time_t t=time(NULL)+46;
  scanChat("%d", &rep);
  count++;
  while(t>time(NULL)){

    if(rep>j) {
      printChat("-\n");
      scanChat("%d",&rep);
      count++;
    }
    else if(rep<j) {
      printChat("+\n");
      scanChat("%d",&rep);
      count++;
    }

    if(rep==j) {
      t1=t;
      break;
    }
  }
  printChat("Votre nombre etait : %d\n", j);

  if(rep>0 && rep<=10000 && rep<(j/2) && rep>(j+j/2) && count<=10 && count >5)
    score[i]=6;
  if(rep>0 && rep<=10000 && rep<(j/2) && rep>(j+j/2) && count<5)
    score[i]=8;
  if(rep>0 && rep<=10000 && rep<(j/2) && rep>(j+j/2) && count>10 && count<=30)    //rep dans l'intervalle le plus eloigne du nombre genere et nombre de tentative
    score[i]=4;
  if(rep>0 && rep<=10000 && rep<(j/2) && rep>(j+j/2) && count>30)
    score[i]=2;

  if(rep>(j/2) && rep<(j+j/2) && rep<(j/4) && rep>(j+j/4) && count<10 && count >5)
    score[i]=15;
  if(rep>(j/2) && rep<(j+j/2) && rep<(j/4) && rep>(j+j/4) && count<5)
    score[i]=20;
  if(rep>(j/2) && rep<(j+j/2) && rep<(j/4) && rep>(j+j/4) && count>10 && count<=30)       //rep dans un intervalle moins eloigne du nombre genere et nombre de tentative
    score[i]=10;
  if(rep>(j/2) && rep<(j+j/2) && rep<(j/4) && rep>(j+j/4) && count>30)
    score[i]=8;

  if(rep>(j/4) && rep<(j+j/4) && rep!=j && count<=10 && count >5)
    score[i]=35;
  if(rep>(j/4) && rep<(j+j/4) && rep!=j && count<5)
    score[i]=40;
  if(rep>(j/4) && rep<(j+j/4) && rep!=j && count>10 && count<30)             //rep dans l'intervalle le moins eloigne du nombre genere et nombre de tentative
    score[i]=30;
  if(rep>(j/4) && rep<(j+j/4) && rep!=j && count>=30)
    score[i]=25;

  if(rep==j && t1<30 && t1>15)
    score[i]=45;
  if(rep==j && t1<15 && t1>5)
    score[i]=50;
  if(rep==j && t1<5 && t1>2)                   //rep trouve et temps restant osef du nombre de tentatives a ce niveau
    score[i]=55;
  if(rep==j && t1<2)
    score[i]=60;

  }
  for (int l = nbJoueurs; l < 4 ; l++)
  {
    switch (difficulty){
    case 1:
      score[l] = time(NULL)%20 ;
      break;
    case 2:
      score[l] = time(NULL)%(40-20)+20;                                //score aleatoire des bots en fonction de la difficulte
      break;
    case 3:
      score[l] = time(NULL)%(60-30)+30;;
      break;
    }
  }


return score;
}
