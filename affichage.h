#ifndef affichage_h
#define affichage_h
#include <curses.h>
#include <stdarg.h>
#include "plateau.h"

void initChat();
void initEtat();
void afficherEtat(Plateau a, int t);
void checkAffichageChat();
void clearAffichageChat();
void delChat();
void delEtat();
void printChat(const char *string, ...);
int scanChat(const char *string, ...);
int lancerDesJoueur(Joueur j);
int barmenu(const char **array, const int row, const int col, const int arraylength, const int width, int menulength, int selection);
int menuPrincipal();
void texteDansCadre(char* text); //ajoute l'affichage du texte dans un joli cadre
void def_couleurs();// à utiliser en début de programme
void miniCase(char c, int x, int y); // affiche une case du plateau
void caseTP(int x,int y); //affiche une case TP (éwé)
void printBoard(Plateau *p,int a, int b);
void afficherSauvegarde(int current);
int selectionPlateau();
int afficherBoardPic(int selection);
void choixModeDeJeux(Plateau *p);
void tutoriel();
#endif
