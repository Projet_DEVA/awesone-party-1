#include <stdio.h>
#include <stdlib.h>
#include "joueur.h"

Joueur *creerJoueurs(int nb, Case *caseDepart)
{
  int i, j;
  Joueur *joueurs = (Joueur *)malloc(sizeof(Joueur) * 4);
  for (i = 0; i < nb; i++)
  {
    joueurs[i].numero = i + 1;
    joueurs[i].inventaire = (Objet *)malloc(sizeof(Objet) * 3);
    for (j = 0; j < 3; j++)
    {
      joueurs[i].inventaire[j].id = nothing;
    }
    joueurs[i].argent = 10;
    joueurs[i].position = caseDepart;
    joueurs[i].nom = (char *)calloc(256, sizeof(char));
    joueurs[i].nbObjet = 0;
    joueurs[i].etoiles = 0;
    joueurs[i].bot = 0;
  }
  for (i = 0; i < 4 - nb; i++)
  {
    joueurs[i + nb].numero = i + 1 + nb;
    joueurs[i + nb].inventaire = (Objet *)malloc(sizeof(Objet) * 3);
    for (j = 0; j < 3; j++)
    {
      joueurs[i + nb].inventaire[j].id = nothing;
    }
    joueurs[i + nb].argent = 10;
    joueurs[i + nb].position = caseDepart;
    joueurs[i + nb].nbObjet = 0;
    joueurs[i + nb].etoiles = 0;
    joueurs[i + nb].bot = 1;
    joueurs[i + nb].nom = (char *)calloc(256, sizeof(char));
    joueurs[i + nb].nom[0] = 'B';
    joueurs[i + nb].nom[1] = 'o';
    joueurs[i + nb].nom[2] = 't';
    joueurs[i + nb].nom[3] = '1' + i;
    joueurs[i + nb].nom[4] = '\0';
  }
  return joueurs;
}

void supprimerJoueur(Joueur *joueurs, int nbjoueurs)
{
  int i;
  for (i = 0; i < nbjoueurs; i++)
  {
    if (joueurs[i].inventaire != NULL)
      free(joueurs[i].inventaire);
    if (joueurs[i].nom != NULL)
      free(joueurs[i].nom);
    joueurs[i].position = 0;
    joueurs[i].numero = 0;
    joueurs[i].argent = 0;
  }
  free(joueurs);
}

void plus(Joueur *joueur, int nbArgent)
{
  joueur->argent += nbArgent;
}

void moins(Joueur *joueur, int nbArgent)
{
  joueur->argent -= nbArgent;
  if (joueur->argent < 0)
    joueur->argent = 0;
}

void prendrePiege(Joueur *joueur, int nbArgent)
{
  int val = rand() % 2;
  moins(joueur, nbArgent * 5);
  if (val == 0)
  {
    changerTypeCase(joueur->position, gain);
  }
  else
  {
    changerTypeCase(joueur->position, perte);
  }
}

int AchatObjet(Joueur *joueur, Objet choix)
{
  if (joueur->nbObjet >= 3)
  {
    printChat("Vous n'avez plus de place dans votre inventaire ! \n");
    return 0;
  }
  else if (joueur->argent < choix.prix)
  {
    printChat("Vous n'avez pas assez d'argent pour acheter cet objet :c !\n");
    return 0;
  }
  joueur->argent -= choix.prix;
  joueur->inventaire[joueur->nbObjet] = choix;
  joueur->nbObjet++;
  printChat("Felicitation ! Vous venez d'acheter l'objet %s !\n", choix.nomObjet);
  return 1;
}

void magasin(Joueur *joueur)
{
  clearAffichageChat();
  printChat("Joueur %s, \n", joueur->nom);
  printChat("Bienvenue au magasin ! Entrez le numéro de l'objet que vous souhaitez acheter, ou entrez '-1' si vous souhaitez sortir du magasin.\n");
  Objet *O = (Objet *)malloc(sizeof(Objet) * 3);
  int c, i;
  enum idObjet id;
  for (i = 0; i < 3; i++) // Permet de generer 3 objets differents
  {
    id = (enum idObjet)(rand() % 7 + 1);
    if (i == 1 && id == O[i - 1].id)
      i--;
    else if (i == 2 && (id == O[1].id || id == O[0].id))
      i--;
    else
    {
      O[i] = genererObjet(id);
    }
  }
  do
  {
    printChat("Vous avez actuellement %d pieces.\n", joueur->argent);
    for (i = 0; i < 3; i++)
    {
      if (O[i].id != 0)
        printChat("L'Objet %d: %s est au prix de %d pieces\n", i + 1, O[i].nomObjet, O[i].prix);
    }
    if (joueur->bot != 1)
    {
      scanChat("%d", &c);
      if (c != -1 && c < 4 && c > 0 && O[c - 1].id != 0)
      {
        if (AchatObjet(joueur, O[c - 1]))
          supprimerObjet(&O[c - 1]);
      }
    }
    else
    {
      if (O[0].prix <= joueur->argent)
      {
        AchatObjet(joueur, O[0]);
        supprimerObjet(&O[0]);
      }
      else if (O[1].prix <= joueur->argent && rand() % 2 == 0)
      {
        AchatObjet(joueur, O[1]);
        supprimerObjet(&O[1]);
      }
      else if (O[2].prix <= joueur->argent && rand() % 2 == 0)
      {
        AchatObjet(joueur, O[2]);
        supprimerObjet(&O[2]);
      }
      c = -1;
    }
    clearAffichageChat();
  } while (c != -1);
  free(O);
}

int enleverObjet(Joueur *joueur, int index)
{
  int i;
  if (&joueur->inventaire[index].id != nothing)
  {
    supprimerObjet(&joueur->inventaire[index]);
    joueur->nbObjet--;
    for (i = 0; i < 2 && joueur->inventaire[i].id == nothing && joueur->inventaire[i].id != nothing; i++)
    {
      joueur->inventaire[i] = joueur->inventaire[i + 1];
      supprimerObjet(&joueur->inventaire[i + 1]);
      i = 0;
    }
  }
  else
  {
    return 0;
  }
  return 1;
}

void miniJeux(Joueur *joueurs, int nbJoueurs, int difficulty)
{
  char **nomJoueurs = (char **)malloc(sizeof(char *) * 4);
  int i, *classement;

  for (i = 0; i < 4; i++)
  {
    nomJoueurs[i] = joueurs[i].nom;
  }
  classement = lancerMiniJeu(nomJoueurs, nbJoueurs, difficulty);

  for (i = 0; i < 4; i++)
  {
    switch (i)
    {
    case 0:
      joueurs[classement[i]].argent += 12;
      break;
    case 1:
      joueurs[classement[i]].argent += 9;
      break;
    case 2:
      joueurs[classement[i]].argent += 6;
      break;
    case 3:
      joueurs[classement[i]].argent += 3;
      break;
    }
  }

  free(nomJoueurs);
  free(classement);
}

void demanderEtoile(Joueur *joueur, int difficulty)
{
  int prix;
  char c;
  switch (difficulty)
  {
  case 1:
    prix = 15;
    break;
  case 2:
    prix = 20;
    break;
  case 3:
    prix = 25;
    break;
  }
  if (joueur->argent >= prix)
  {
    clearAffichageChat();
    printChat("Joueur %s, voulez vous acheter une etoile ? Y pour oui N pour non\n", joueur->nom);
    printChat("L'etoile vous coutera %d pieces !\n", prix);
    printChat("Vous avez %d pieces\n", joueur->argent);
    if (joueur->bot == 0)
    {
      do
      {
        printChat("Entrez votre reponse :");
        scanChat("%c", &c);
      } while (c != 'N' && c != 'Y');
    }
    else
    {
      if (rand() % 100 > 12)
      {
        c = 'N';
      }
      else
      {
        c = 'Y';
      }
    }
    if (c == 'Y')
    {
      joueur->argent -= prix;
      joueur->etoiles += 1;
      printChat("Felicitation ! Le joueur %s vient d'acquerir une etoile \n", joueur->nom);
    }
  }
  else
  {
    printChat("Le joueur %s n'a pas assez d'argent pour acheter une etoile :c\n", joueur->nom);
  }
  printChat("A bientot ! \n");
}
