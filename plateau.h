#ifndef plateau_h
#define plateau_h
#include "joueur.h"

typedef struct
{
    int nbCases;
    Case *cases;
    Joueur *joueurs;
    int nbJoueurs;
    int difficulty;
    int nbArgentPlus;
    int nbArgentMoins;
} Plateau;

extern void startEvent(Plateau *p);
Plateau importBoard(char* fichier);
void printfBoard(Plateau plateau);
void supprimerPlateau(Plateau *plateau);
void playerTurn(Joueur *j, Plateau *p, int dice);
char enumToChar(enum typecase c);

extern int teleport(Plateau *p, Joueur *j);

extern int voler(Plateau *p, Joueur *j);

extern int reculer(Plateau *p, Joueur *j);

extern void poserPiege(Joueur *j);

extern int perteObjet(Plateau *p, Joueur *j);

extern void startEvent(Plateau *p);

extern void afficherEtat(Plateau a, int t);

void turn(Plateau *p, int t);

void classement(Plateau *p);

void sauvegarde(Plateau *p);

Plateau *charger(int *nb);

#endif
