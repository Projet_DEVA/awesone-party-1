CFLAGS=-Wall -Wextra -lncurses
OBJECT=	case.o joueur.o plateau.o spamMe.o main.o miniJeu.o shop.o event.o objet.o affichage.o plusmoins.o dragon.o
TITLE= AwesomeParty.out

all : $(OBJECT)
		gcc $(OBJECT) -o $(TITLE) $(CFLAGS) -g

final : $(OBJECT)
		gcc $(OBJECT) -o $(TITLE) $(CFLAGS) -O2

%.o : %.c
		gcc -c $^ $(CFLAGS) 

clean :
		rm *.o MiniJeu/*.o -rf

cleanall :
		rm *.o MiniJeu/*.o *.out -rf
