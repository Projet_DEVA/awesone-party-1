#include "plateau.h"

Plateau createBoard()
{
  srand(time(NULL));
  Plateau plateau;
  plateau.nbCases = 0;
  plateau.cases = NULL;
  plateau.nbJoueurs = 0;
  plateau.difficulty = 0;
  plateau.nbArgentPlus = 3;
  plateau.nbArgentMoins = 3;
  return plateau;
}

void ajouterCase(Plateau *plateau, char type, int placement)
{
  switch (type)
  {
  case 'D':
    plateau->cases[placement] = creerCase(placement, debut);
    break;
  case 'F':
    plateau->cases[placement] = creerCase(placement, fin);
    break;
  case 'N':
    plateau->cases[placement] = creerCase(placement, nord);
    break;
  case 'E':
    plateau->cases[placement] = creerCase(placement, est);
    break;
  case 'S':
    plateau->cases[placement] = creerCase(placement, sud);
    break;
  case 'X':
    plateau->cases[placement] = creerCase(placement, croisement);
    break;
  case '1':
    plateau->cases[placement] = creerCase(placement, gain);
    break;
  case '0':
    plateau->cases[placement] = creerCase(placement, perte);
    break;
  case 'V':
    plateau->cases[placement] = creerCase(placement, event);
    break;
  case 'M':
    plateau->cases[placement] = creerCase(placement, shop);
    break;
  case 'P':
    plateau->cases[placement] = creerCase(placement, trap);
    break;
  }
}

void supprimerPlateau(Plateau *plateau)
{
  supprimerJoueur(plateau->joueurs, 4);
  plateau->nbCases = 0;
  plateau->nbJoueurs = 0;
  if (plateau->cases != NULL)
    free(plateau->cases);
}


int verifPlateau(Plateau *plateau){
  int i = 0, n = 0, s = 0, e = 0;
  if(plateau == NULL){
    return 0;
  }
  if(plateau->cases[0].type != debut){
    return 0;
  }
  else if(plateau->cases[plateau->nbCases - 1].type != fin){
    return 0;
  }
  else{
    for(i = 0; i < plateau->nbCases; i++){
      if(plateau->cases[i].type == nord && n == 0){
        if(s == 1){
          return 0;
        }
        n = 1;
      }
      if(plateau->cases[i].type == sud && s == 0){
        s = 1;
      }
      if(plateau->cases[i].type == est && (n == 1 || s == 1) && e == 0){
        e = 1;
      }
      if(plateau->cases[i].type == croisement && (n == 1 || s == 1) && e == 1){
        e = 0;
        n = 0;
        s = 0;
      }
    }
    if(n == 1 || s == 1 || e == 1){
      return 0;
    }
      
  }
  return 1;
}

Plateau importBoard(char* fichier)
{
  FILE *f = NULL;
  int i = 0, k = 0, verif;
  Plateau plateau = createBoard();
  char chaine[4096];
  f = fopen(fichier, "r");
  fgets(chaine, 4096, f);
  plateau.nbCases = atoi(chaine);
  plateau.cases = (Case *)malloc(sizeof(Case) * (plateau.nbCases + 1));
  while (fgets(chaine, 4096, f) != NULL)
  {
    while (chaine[i] != '\0' && chaine[i] != '\n')
    {
      ajouterCase(&plateau, chaine[i], k);
      k++;
      i++;
    }
    i = 0;
  }
  fclose(f);
  verif = verifPlateau(&plateau);
  if(verif == 0){
    f = NULL;
    free(plateau.cases);
    plateau.nbCases = -1;
    i = 0;
    k = 0;
  }
  return plateau;
}

void printfBoard(Plateau plateau)
{
  int i;
  for (i = 0; i < plateau.nbCases; i++)
  {
    switch (plateau.cases[i].type)
    {
    case debut:
      printChat("D");
      break;
    case fin:
      printChat("F");
      break;
    case nord:
      printChat("N");
      break;
    case est:
      printChat("E");
      break;
    case sud:
      printChat("S");
      break;
    case croisement:
      printChat("X");
      break;
    case gain:
      printChat("1");
      break;
    case perte:
      printChat("0");
      break;
    case event:
      printChat("V");
      break;
    case shop:
      printChat("M");
      break;
    case trap:
      printChat("P");
      break;
    }
  }
  printChat("\n");
}

enum typecase charToEnum(char c)
{
  switch (c)
  {
  case 'N':
    return nord;
  case 'S':
    return sud;
  case 'E':
    return est;
  default:
    return nord;
  }
}

char enumToChar(enum typecase c)
{
  switch (c)
  {
  case nord:
    return 'N';
  case sud:
    return 'S';
  case est:
    return 'E';
  case debut:
    return 'D';
  case fin:
    return 'F';
  case croisement:
    return 'X';
  case gain:
    return '1';
  case perte:
    return '0';
  case event:
    return 'V';
  case shop:
    return 'M';
  case trap:
    return 'P';
  default :
    return '\0';
  }
}

const char *charToStringCase(char type)
{
  switch (type)
  {
  case 'N':
    return "Nord";
  case 'S':
    return "Sud";
  case 'E':
    return "Est";
  default:
    return "Nord";
  }
}

char demanderDirection(char *types, Joueur *j)
{
  WINDOW *w, *winjoueur;
  int nbDirection = 0, i, ch;
  while (types[nbDirection] != '\0' && nbDirection < 4)
  {
    nbDirection++;
  }
  w = subwin(stdscr,2 + nbDirection, 8, 3, 1);
  winjoueur = subwin(stdscr,3, 60, 0, 1);
  box(w, 0, 0);
  mvwprintw(winjoueur, 2, 2, "Joueur %s, choississez votre direction : ", j->nom);
  for (i = 0; i < nbDirection; i++)
  {
    if (i == 0)
      wattron(w, A_STANDOUT);
    else
      wattroff(w, A_STANDOUT);
    mvwprintw(w, i + 1, 1, "%s", charToStringCase(types[i]));
  }
  wrefresh(w);
  wrefresh(winjoueur);

  i = 0;
  noecho();
  keypad(w, TRUE);
  curs_set(0);
  while ((ch = wgetch(w)) != '\n')
  {
    mvwprintw(w, i + 1, 1, "%s", charToStringCase(types[i]));
    switch (ch)
    {
    case KEY_UP:
      i--;
      i = (i < 0) ? nbDirection - 1 : i;
      break;
    case KEY_DOWN:
      i++;
      i = (i > nbDirection - 1) ? 0 : i;
      break;
    }
    wattron(w, A_STANDOUT);

    mvwprintw(w, i + 1, 1, "%s", charToStringCase(types[i]));
    wattroff(w, A_STANDOUT);
  }
  //werase(winjoueur);
  //werase(w);
  refresh();
  wclear(w);
  wclear(winjoueur);
  wrefresh(w);
  wrefresh(winjoueur);
  delwin(w);
  delwin(winjoueur);
  echo();
  refresh();
  ch = types[i];
  free(types);
  return ch;
}

char *getPosibilite(Joueur *j, Plateau *p)
{
  char *type = (char *)malloc(sizeof(char *) * 4);
  int i = j->position->indiceTab;
  int k = 0;
  while (p->cases[i].type != croisement)
  {
    if (p->cases[i].type == nord || p->cases[i].type == sud || p->cases[i].type == est)
    {
      type[k] = enumToChar(p->cases[i].type);
      k++;
    }
    i++;
  }
  while (k < i)
  {
    type[k] = '\0';
    k++;
  }
  return type;
}

void deplacementDansBranche(Joueur *j, Plateau *p)
{
  bool temp = 0;
  char ptcardinal, *tempchar;
  int tempInt = j->position->indiceTab - 1, lgchar;
  while (p->cases[tempInt].type != debut && p->cases[tempInt].type != croisement && temp == 0)
  {
    if (p->cases[tempInt].type == nord || p->cases[tempInt].type == sud || p->cases[tempInt].type == est)
    {
      temp = 1;
    }
    else
    {
      tempInt--;
    }
  }
  if (temp)
  {
    for (int i = j->position->indiceTab; i < p->nbCases; i++)
    {
      if (p->cases[i].type == croisement)
      {
        j->position = &p->cases[i];
        i = p->nbCases;
      }
    }
  }
  else
  {
    if (j->bot == 0)
    {
      ptcardinal = demanderDirection(getPosibilite(j, p), j);
    }
    else
    {
      tempchar = getPosibilite(j, p);
      lgchar = strlen(tempchar);
      ptcardinal = tempchar[rand() % lgchar];
    }
    enum typecase type = charToEnum(ptcardinal);
    for (int i = j->position->indiceTab; i < p->nbCases; i++)
    {
      if (p->cases[i].type == type)
      {
        j->position = &p->cases[i];
        i = p->nbCases;
      }
    }
  }
}

void playerTurn(Joueur *j, Plateau *p, int dice)
{
  int cpt = 0;
  while (cpt < dice)
  {
    switch (j->position->type)
    {
    case debut:
      j->position = &p->cases[j->position->indiceTab + 1];
      cpt++; //renvoyer apres la caseDepart
      break;
    case fin:
      demanderEtoile(j, p->difficulty);
      j->position = &p->cases[0]; //renvoyer au debut du plateau
      break;
    case nord:
      deplacementDansBranche(j, p);
      j->position = &p->cases[j->position->indiceTab + 1];
      //suivre la suite du tableau faire attention lettre suivante possible
      break;
    case est:
      deplacementDansBranche(j, p); //suivre la suite du tableau faire attention lettre suivante possible
      j->position = &p->cases[j->position->indiceTab + 1];
      break;
    case sud:
      deplacementDansBranche(j, p); //suivre la suite du tableau faire attention lettre suivante possible
      j->position = &p->cases[j->position->indiceTab + 1];
      break;
    case croisement:
      j->position = &p->cases[j->position->indiceTab + 1];
      break;
    case shop:
      magasin(j);
      j->position = &p->cases[j->position->indiceTab + 1];
      cpt++;
      break;
    default:
      cpt++;
      j->position = &p->cases[j->position->indiceTab + 1];
    }
  }
  switch (p->cases[j->position->indiceTab].type)
  {
  case gain:
    plus(j, p->nbArgentPlus);
    break;
  case perte:
    moins(j, p->nbArgentMoins);
    break;
  case event:
    startEvent(p);
    break;
  case trap:
    prendrePiege(j, p->nbArgentMoins);
    break;
  case shop:
    magasin(j);
    break;
  default:
    break;
  }
}

int useObjet(Joueur *j, Plateau *p)
{
  int i, val, k, nb = 0;
  if (j->nbObjet == 0)
    return 0;
  if (j->bot == 0)
  {
    clearAffichageChat();
    do
    {
      printChat("Joueurs %s, ", j->nom);
      printChat("Entrez le numero de  l'objet que vous voulez utiliser : (-1 pour sortir) \n");
      for (i = 0; i < j->nbObjet; i++)
      {
        printChat("Objet %d : %s\n", i + 1, j->inventaire[i].nomObjet);
      }
      scanChat("%d", &val);
      if(j->inventaire[i].id == piege && (j->position->type = gain || j->position->type == perte))
        printChat("Vous ne pouvez pas mettre un piege que sur des cases normales\n");
    } while (val != -1 && (val > j->nbObjet || val <= 0 || j->inventaire[i].id == piege) && (val > j->nbObjet || val <= 0 || j->inventaire[i].id != piege || j->position->type != gain || j->position->type != perte));
  }
  else
  {
    val = rand() % (j->nbObjet) + 1;
    if (val == j->nbObjet + 1)
      return 0;
  }
  if (val != -1)
  {
    switch (j->inventaire[val - 1].id)
    {
    case teleporteur:
      k = teleport(p, j);
      break;
    case vol:
      k = voler(p, j);
      break;
    case recule:
      k = reculer(p, j);
      break;
    case piege:
      poserPiege(j);
      k = 1;
      break;
    case loose:
      k = perteObjet(p, j);
      break;
    case de2:
      nb = lancerDesJoueur(*j);
      k = 1;
      break;
    case de3:
      nb = (lancerDesJoueur(*j) + lancerDesJoueur(*j));
      k = 1;
      break;
    case nothing:
      break;
    }
    if(k == 1){
      enleverObjet(j, val);
    }
  }
  return nb;
}

void turn(Plateau *p, int t)
{
  int i, val;
  for (i = 0; i < 4; i++)
  {
    val = useObjet(&p->joueurs[i], p);
    afficherEtat(*p, t);
    playerTurn(&p->joueurs[i], p, lancerDesJoueur(p->joueurs[i]) + val);
    afficherEtat(*p, t);
  }
}

void classement(Plateau *p)
{
  Joueur t;
  int i, j;
  for (i = 1; i < 4; i++)
  {
    t = p->joueurs[i];
    for (j = i; j > 0 && (p->joueurs[j - 1].etoiles < t.etoiles || (p->joueurs[j - 1].etoiles == t.etoiles && p->joueurs[j - 1].argent < t.argent)); j--)
    {
      p->joueurs[j] = p->joueurs[j - 1];
    }
    p->joueurs[j] = t;
  }
}

void sauvegarde(Plateau *p)
{
  int i = 0;
  FILE *f;
  f = fopen("classement.sav", "ab");
  if (f != NULL)
  {
    classement(p);
    fwrite(p, sizeof(Plateau), 1, f);
    for (i = 0; i < 4; i++)
    {
      fwrite(&p->joueurs[i], sizeof(Joueur), 1, f);
      fwrite(p->joueurs[i].nom, sizeof(char), 256, f);
    }
    fclose(f);
  }
  else
  {
    printf("Erreur\n");
  }
}

Plateau *charger(int *nb)
{
  int n, i = 0, k;
  Plateau *p = NULL;
  void *temp = malloc(sizeof(Plateau) + (sizeof(Joueur) * 4) + (sizeof(char) * 256 * 4));
  FILE *f;
  f = fopen("classement.sav", "rb");
  if (f != NULL)
  {
    while (fread(temp, sizeof(Plateau) + (sizeof(Joueur) * 4) + (sizeof(char) * 256 * 4), 1, f) == 1)
    {
      i++;
    }
    p = (Plateau *)malloc(sizeof(Plateau) * i);
    fseek(f, 0, SEEK_SET);
    for (k = 0; k < i; k++)
    {
      fread(&p[k], sizeof(Plateau), 1, f);
      p[k].cases = NULL;
      p[k].joueurs = (Joueur *)malloc(sizeof(Joueur) * 4);
      for (n = 0; n < 4; n++)
      {
        fread(&p[k].joueurs[n], sizeof(Joueur), 1, f);
        p[k].joueurs[n].nom = (char *)malloc(sizeof(char) * 256);
        fread(p[k].joueurs[n].nom, sizeof(char), 256, f);
        p[k].joueurs[n].inventaire = NULL;
        p[k].joueurs[n].position = NULL;
      }
    }
    *nb = i;
    fclose(f);
  }
  free(temp);
  return p;
}
