# Projet DEVA : Awesome party 1

Ce dépot contient le jeu Awesome party 1 du projet DEVA de 2018.

## Introduction

Ces instructions vont vous guider pour télécharger et faire le faire fonctionner sur vos machines.

### Prérequis

Voici ce dont vous aurez besoin pour travailler sur le projet :

    - Un compilateur c (gcc sous Linux ou MinGW sous windows)
    - La libraire ncurses (fourni sur ce git)

### Télécharger le projet 

Pour télécharger les **sources** du projet, ouvrez une console, rendez-vous dans le répertoire de votre choix, et clonez ce dépot via la commande
```
git clone https://gitlab.com/Projet_DEVA/awesone-party-1.git
```

### Installation et lancement

Voici les étapes à suivre pour installer le projet sur vos machine 

Rendez-vous à la racine du projet et ouvrez le dossier source
```
cd awesome-party-1
```

Lancez la commande
```
make
```

Lancez le jeu.

**Attention :** Pour démarrer une nouvelle partie, un dossier map doit être présent avec un moins une map dedans, sinon au lancement
d'une nouvelle partie le programme reviendra au menu principal

### Creation de map

Pour créer une nouvelle carte du jeu il faut créer un nouveau fichier map#.txt dans le dossier map où # est remplacer par le numéro de la map.
Il faut que le numéro de la map soit la suivante de la carte ayant le numéro le plus grand.

#### Exemple :
```
Si map1.txt et map2.txt sont présent dans le dossier il faudra créer map3.txt.
```

Ensuite pour la création de la carte il faut suivres quelques règles : 

- La première case de la carte doit être le caractère D.
- La dernière case de la carte doit être le caractère F.
- Une case qui ajoute de l'argent doit être le caractère 1.
- Une case qui enlève de l'argent doit être le caractère 0.
- Une case qui lance un événement doit être le caractère V.
- Une case qui lance un magasin doit être le caractère M.

Si vous voulez faire une embranchement :

L'embranchement doit commencer par le caractère N ou S représentant respectivement le chemin Nord ou Sud.
Il doit obligatoirement contenir le caracatere E qui represente le chemin Est.
Il doit finir par le caractere X qui représente le croisement de ces embranchements.

Le fichier map.txt doit contenir le nombre de cases qui seront présentes dans le plateau.

Tout autre caractère sera ignoré.

#### Exemple : 
```
38
D1010101010101010N10MS10ME10MX1010101F
```


## Créé avec 

* [gcc](https://gcc.gnu.org/) - Compilateur C pour Linux
* [MinGW](http://mingw.org/) - Compilateur C pour Windows
* ncurses - Libraire d'affichage console pour Linux
* [pdcurses](https://pdcurses.sourceforge.io/) - Libraire d'affichage console pour Windows

## Contributeurs

* **Romane Merveaux**
* **Gabriel Loiseau**
* **Hugo Haquette**

##Licence

GNU GENERAL PUBLIC LICENSE Version 3 
