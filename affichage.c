#include "affichage.h"

WINDOW *chat;
WINDOW *etat;
WINDOW *board;

int lancerDesJoueur(Joueur j)
{
  WINDOW *des;
  int dice = 3, temp = 50, c = 4;
  noecho();
  curs_set(0);
  des = subwin(stdscr, 17, 25, 0, COLS - 25);
  //box(des, ACS_VLINE, ACS_HLINE);
  wrefresh(des);
  mvwprintw(des, 1, 9, "Joueur %s : ", j.nom);
  if (j.bot == 0)
  {
    keypad(stdscr, TRUE);
    do
    {
      timeout(30);
    } while (getch() != ERR);
    do
    {
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /               /|");
      mvwprintw(des, 5, 2, "  /       O       / |");
      mvwprintw(des, 6, 2, " /               /  |");
      mvwprintw(des, 7, 2, "/_______________/  O|");
      mvwprintw(des, 8, 2, "|               |   |");
      mvwprintw(des, 9, 2, "|  O            |   |");
      mvwprintw(des, 10, 2, "|               |   |");
      mvwprintw(des, 11, 2, "|       O       |   /");
      mvwprintw(des, 12, 2, "|               |O /");
      mvwprintw(des, 13, 2, "|            O  | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      dice = 3;
      napms(temp);
      timeout(30);
      if (getch() != ERR)
        break;
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /   O        O  /|");
      mvwprintw(des, 5, 2, "  /       O       / |");
      mvwprintw(des, 6, 2, " /  O        O   / O|");
      mvwprintw(des, 7, 2, "/_______________/   |");
      mvwprintw(des, 8, 2, "|               |O O|");
      mvwprintw(des, 9, 2, "|  O         O  |   |");
      mvwprintw(des, 10, 2, "|               |O O|");
      mvwprintw(des, 11, 2, "|               |   /");
      mvwprintw(des, 12, 2, "|               |O /");
      mvwprintw(des, 13, 2, "|  O         O  | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      dice = 4;
      napms(temp);
      timeout(30);
      if (getch() != ERR)
        break;
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /           O   /|");
      mvwprintw(des, 5, 2, "  /       O       / |");
      mvwprintw(des, 6, 2, " /   O           /  |");
      mvwprintw(des, 7, 2, "/_______________/   |");
      mvwprintw(des, 8, 2, "|               |   |");
      mvwprintw(des, 9, 2, "|  O         O  | O |");
      mvwprintw(des, 10, 2, "|               |   |");
      mvwprintw(des, 11, 2, "|       O       |   /");
      mvwprintw(des, 12, 2, "|               |  /");
      mvwprintw(des, 13, 2, "|  O         O  | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      dice = 5;
      napms(temp);
      timeout(30);
      if (getch() != ERR)
        break;
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /           O   /|");
      mvwprintw(des, 5, 2, "  /               / |");
      mvwprintw(des, 6, 2, " /   O           / O|");
      mvwprintw(des, 7, 2, "/_______________/   |");
      mvwprintw(des, 8, 2, "|               |O  |");
      mvwprintw(des, 9, 2, "|  O         O  |   |");
      mvwprintw(des, 10, 2, "|               |  O|");
      mvwprintw(des, 11, 2, "|  O         O  |   /");
      mvwprintw(des, 12, 2, "|               |O /");
      mvwprintw(des, 13, 2, "|  O         O  | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      dice = 6;
      napms(temp);
      timeout(30);
      if (getch() != ERR)
        break;
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /   O       O   /|");
      mvwprintw(des, 5, 2, "  /               / |");
      mvwprintw(des, 6, 2, " /  O       O    / O|");
      mvwprintw(des, 7, 2, "/_______________/   |");
      mvwprintw(des, 8, 2, "|               |O  |");
      mvwprintw(des, 9, 2, "|               | O |");
      mvwprintw(des, 10, 2, "|               |  O|");
      mvwprintw(des, 11, 2, "|       O       |   /");
      mvwprintw(des, 12, 2, "|               |O /");
      mvwprintw(des, 13, 2, "|               | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      dice = 1;
      napms(temp);
      timeout(30);
      if (getch() != ERR)
        break;
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /  O    O    O  /|");
      mvwprintw(des, 5, 2, "  /               / |");
      mvwprintw(des, 6, 2, " /  O    O    O  / O|");
      mvwprintw(des, 7, 2, "/_______________/   |");
      mvwprintw(des, 8, 2, "|               |   |");
      mvwprintw(des, 9, 2, "|  O            | O |");
      mvwprintw(des, 10, 2, "|               |   |");
      mvwprintw(des, 11, 2, "|               |   /");
      mvwprintw(des, 12, 2, "|               |O /");
      mvwprintw(des, 13, 2, "|            O  | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      dice = 2;
      napms(temp);
      timeout(30);
      if (getch() != ERR)
        break;
    } while (1);
    keypad(stdscr, FALSE);
  }
  else
  {
    dice = rand() % 6 + 1;
    do
    {
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /               /|");
      mvwprintw(des, 5, 2, "  /       O       / |");
      mvwprintw(des, 6, 2, " /               /  |");
      mvwprintw(des, 7, 2, "/_______________/  O|");
      mvwprintw(des, 8, 2, "|               |   |");
      mvwprintw(des, 9, 2, "|  O            |   |");
      mvwprintw(des, 10, 2, "|               |   |");
      mvwprintw(des, 11, 2, "|       O       |   /");
      mvwprintw(des, 12, 2, "|               |O /");
      mvwprintw(des, 13, 2, "|            O  | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      napms(temp + 30);
      if (dice == 3 && c == 0)
        break;
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /   O        O  /|");
      mvwprintw(des, 5, 2, "  /       O       / |");
      mvwprintw(des, 6, 2, " /  O        O   / O|");
      mvwprintw(des, 7, 2, "/_______________/   |");
      mvwprintw(des, 8, 2, "|               |O O|");
      mvwprintw(des, 9, 2, "|  O         O  |   |");
      mvwprintw(des, 10, 2, "|               |O O|");
      mvwprintw(des, 11, 2, "|               |   /");
      mvwprintw(des, 12, 2, "|               |O /");
      mvwprintw(des, 13, 2, "|  O         O  | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      napms(temp + 30);
      if (dice == 4 && c == 0)
        break;
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /           O   /|");
      mvwprintw(des, 5, 2, "  /       O       / |");
      mvwprintw(des, 6, 2, " /   O           /  |");
      mvwprintw(des, 7, 2, "/_______________/   |");
      mvwprintw(des, 8, 2, "|               |   |");
      mvwprintw(des, 9, 2, "|  O         O  | O |");
      mvwprintw(des, 10, 2, "|               |   |");
      mvwprintw(des, 11, 2, "|       O       |   /");
      mvwprintw(des, 12, 2, "|               |  /");
      mvwprintw(des, 13, 2, "|  O         O  | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      napms(temp + 30);
      if (dice == 5 && c == 0)
        break;
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /           O   /|");
      mvwprintw(des, 5, 2, "  /               / |");
      mvwprintw(des, 6, 2, " /   O           / O|");
      mvwprintw(des, 7, 2, "/_______________/   |");
      mvwprintw(des, 8, 2, "|               |O  |");
      mvwprintw(des, 9, 2, "|  O         O  |   |");
      mvwprintw(des, 10, 2, "|               |  O|");
      mvwprintw(des, 11, 2, "|  O         O  |   /");
      mvwprintw(des, 12, 2, "|               |O /");
      mvwprintw(des, 13, 2, "|  O         O  | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      napms(temp + 30);
      if (dice == 6 && c == 0)
        break;
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /   O       O   /|");
      mvwprintw(des, 5, 2, "  /               / |");
      mvwprintw(des, 6, 2, " /  O       O    / O|");
      mvwprintw(des, 7, 2, "/_______________/   |");
      mvwprintw(des, 8, 2, "|               |O  |");
      mvwprintw(des, 9, 2, "|               | O |");
      mvwprintw(des, 10, 2, "|               |  O|");
      mvwprintw(des, 11, 2, "|       O       |   /");
      mvwprintw(des, 12, 2, "|               |O /");
      mvwprintw(des, 13, 2, "|               | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      napms(temp + 30);
      if (dice == 1 && c == 0)
        break;
      mvwprintw(des, 3, 2, "    .---------------.");
      mvwprintw(des, 4, 2, "   /  O    O    O  /|");
      mvwprintw(des, 5, 2, "  /               / |");
      mvwprintw(des, 6, 2, " /  O    O    O  / O|");
      mvwprintw(des, 7, 2, "/_______________/   |");
      mvwprintw(des, 8, 2, "|               |   |");
      mvwprintw(des, 9, 2, "|  O            | O |");
      mvwprintw(des, 10, 2, "|               |   |");
      mvwprintw(des, 11, 2, "|               |   /");
      mvwprintw(des, 12, 2, "|               |O /");
      mvwprintw(des, 13, 2, "|            O  | /");
      mvwprintw(des, 14, 2, "|               |/");
      mvwprintw(des, 15, 2, "'---------------'");
      wrefresh(des);
      napms(temp + 30);
      if (dice == 2 && c == 0)
        break;
      c--;
    } while (1);
  }
  napms(2000); //temps durant lequel le dés reste affiché
  wclear(des);
  curs_set(1);
  wrefresh(des);
  echo();
  return dice;
}

int menuPrincipal()
{
  int selection, arraylength = 4, width = 15, menulength = 4, i = -6;
  const char *testarray[] = {"Tutoriel", "Nouvelle Partie", "Historique", "Quitter"};
  initscr();
  noecho();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 + i++, COLS / 2 - 42, "    ___                                             ____             __           ___");
  mvprintw(LINES / 2 + i++, COLS / 2 - 42, "   /   |_      _____  _________  ____ ___  ___     / __ \\____ ______/ /___  __   <  /");
  mvprintw(LINES / 2 + i++, COLS / 2 - 42, "  / /| | | /| / / _ \\/ ___/ __ \\/ __ `__ \\/ _ \\   / /_/ / __ `/ ___/ __/ / / /   / / ");
  mvprintw(LINES / 2 + i++, COLS / 2 - 42, " / ___ | |/ |/ /  __(__  ) /_/ / / / / / /  __/  / ____/ /_/ / /  / /_/ /_/ /   / /  ");
  mvprintw(LINES / 2 + i++, COLS / 2 - 42, "/_/  |_|__/|__/\\___/____/\\____/_/ /_/ /_/\\___/  /_/    \\__,_/_/   \\__/\\__, /   /_/");
  mvprintw(LINES / 2 + i++, COLS / 2 - 42, "                                                                     /____/          ");
  mvprintw(LINES / 4, COLS / 8, "--*");
  mvprintw(LINES / 6, COLS / 4, "--*");
  mvprintw(LINES / 10, COLS / 10, "--*");
  mvprintw(LINES / 2, COLS / 10, "--*");
  mvprintw(LINES - LINES / 4, COLS - COLS / 4, "--*");
  mvprintw(LINES - LINES / 4, COLS / 4, "--*");
  mvprintw(LINES - LINES / 3, COLS / 3, ">>*");
  mvprintw(LINES / 2 + 1, COLS - COLS / 4, ">>*");
  mvprintw(LINES / 3 - 2, COLS - COLS / 3, ">>*");
  mvprintw(LINES / 4, COLS / 2, ">>*");
  i++;
  mvprintw(LINES / 2 + i++, COLS / 2 - 11, "--- Menu ---");

  keypad(stdscr, TRUE);

  selection = barmenu(testarray, LINES / 2 + ++i, COLS / 2 - 11, arraylength, width, menulength, 0);
  keypad(stdscr, FALSE);
  clear();
  refresh();
  echo();
  curs_set(1);
  endwin();
  return selection;
}
//définition de barmenu et barmenuBis
int barmenu(const char **array, const int row, const int col, const int arraylength, const int width, int menulength, int selection)
{
  int counter, offset = 0, ky = 0;
  char formatstring[7];
  curs_set(0);

  if (arraylength < menulength)
    menulength = arraylength;

  if (selection > menulength)
    offset = selection - menulength + 1;

  sprintf(formatstring, "%%-%ds", width);

  while (1)
  {
    for (counter = 0; counter < menulength; counter++)
    {
      if (counter + offset == selection)
        attron(A_REVERSE);
      mvprintw(row + counter, col, formatstring, array[counter + offset]);
      attroff(A_REVERSE);
    }

    ky = getch();

    switch (ky)
    {
    case KEY_UP:
      if (selection)
      {
        selection--;
        if (selection < offset)
          offset--;
      }
      break;
    case KEY_DOWN:
      if (selection < arraylength - 1)
      {
        selection++;
        if (selection > offset + menulength - 1)
          offset++;
      }
      break;
    case KEY_HOME:
      selection = 0;
      offset = 0;
      break;
    case KEY_END:
      selection = arraylength - 1;
      offset = arraylength - menulength;
      break;
    case KEY_PPAGE:
      selection -= menulength;
      if (selection < 0)
        selection = 0;
      offset -= menulength;
      if (offset < 0)
        offset = 0;
      break;
    case KEY_NPAGE:
      selection += menulength;
      if (selection > arraylength - 1)
        selection = arraylength - 1;
      offset += menulength;
      if (offset > arraylength - menulength)
        offset = arraylength - menulength;
      break;
    case 10:
      return selection;
      break;
    case KEY_F(1):
      return -1;
    }
  }
  return -1;
}

int barmenuBis(const char array[256][50], const int row, const int col, const int arraylength, const int width, int menulength, int selection)
{
  int counter, offset = 0, ky = 0, verif;
  char formatstring[7];
  curs_set(0);

  if (arraylength < menulength)
    menulength = arraylength;

  if (selection > menulength)
    offset = selection - menulength + 1;

  sprintf(formatstring, "%%-%ds", width);
  verif = afficherBoardPic(selection);
  while (1)
  {
    for (counter = 0; counter < menulength; counter++)
    {
      if (counter + offset == selection)
      {
        attron(A_REVERSE);
        if (verif == 0)
          attrset(COLOR_PAIR(2) | A_BOLD);
      }
      mvprintw(row + counter, col, formatstring, array[counter + offset]);
      attrset(0);
      attroff(A_REVERSE);
    }
    ky = getch();

    switch (ky)
    {
    case KEY_UP:
      if (selection)
      {
        selection--;
        if (selection < offset)
          offset--;
      }
      verif = afficherBoardPic(selection);
      break;
    case KEY_DOWN:
      if (selection < arraylength - 1)
      {
        selection++;
        if (selection > offset + menulength - 1)
          offset++;
      }
      verif = afficherBoardPic(selection);
      break;
    case KEY_HOME:
      selection = 0;
      offset = 0;
      verif = afficherBoardPic(selection);
      break;
    case KEY_END:
      selection = arraylength - 1;
      offset = arraylength - menulength;
      verif = afficherBoardPic(selection);
      break;
    case KEY_PPAGE:
      selection -= menulength;
      if (selection < 0)
        selection = 0;
      offset -= menulength;
      if (offset < 0)
        offset = 0;
      verif = afficherBoardPic(selection);
      break;
    case KEY_NPAGE:
      selection += menulength;
      if (selection > arraylength - 1)
        selection = arraylength - 1;
      offset += menulength;
      if (offset > arraylength - menulength)
        offset = arraylength - menulength;
      break;
    case 10:
      if (verif != 0)
      {
        return selection;
      }
      break;
    }
  }
  return -1;
}

void initChat()
{
  echo();
  chat = subwin(stdscr, LINES / 4, COLS, LINES - (LINES / 4), 0);
  box(chat, ACS_VLINE, ACS_HLINE);
  wrefresh(chat);
}

void initEtat()
{
  echo();
  etat = subwin(stdscr, 7, COLS, LINES - (LINES / 4 + 7), 0);
  box(etat, ACS_VLINE, ACS_HLINE);
  wrefresh(etat);
}

void delChat()
{
  werase(chat);
  delwin(chat);
}

void delEtat()
{
  werase(etat);
  delwin(etat);
}

void afficherEtat(Plateau a, int t)
{
  int i;
  wclear(etat);
  box(etat, ACS_VLINE, ACS_HLINE);
  mvwprintw(etat, 1, 3, "Tour numero %d :", t);
  for (i = 0; i < 4; i++)
  {
    mvwprintw(etat, 2 + i, 3, "Le joueur %d du nom de %s est a la case %d avec %d pieces et %d etoiles", a.joueurs[i].numero, a.joueurs[i].nom, a.joueurs[i].position->indiceTab, a.joueurs[i].argent, a.joueurs[i].etoiles);
  }
  wrefresh(etat);
  printBoard(&a, (LINES - (LINES / 4 + 7)) / 2, COLS / 2 - a.nbCases);
  mvwprintw(board, LINES / 3 + 5, 2, "1 : Gain");
  wattrset(board, COLOR_PAIR(1) | A_BOLD);
  mvwprintw(board, LINES / 3 + 5, 2, "1");
  wattrset(board, 0);
  mvwprintw(board, LINES / 3 + 5, 12, "0 : Perte");
  wattrset(board, COLOR_PAIR(2) | A_BOLD);
  mvwprintw(board, LINES / 3 + 5, 12, "0");
  wattrset(board, 0);
  mvwprintw(board, LINES / 3 + 5, 23, "M : Magasin");
  wattrset(board, COLOR_PAIR(4) | A_BOLD);
  mvwprintw(board, LINES / 3 + 5, 23, "M");
  wattrset(board, 0);
  mvwprintw(board, LINES / 3 + 5, 36, "V : Evenement");
  wattrset(board, COLOR_PAIR(3) | A_BOLD);
  mvwprintw(board, LINES / 3 + 5, 36, "V");
  wattrset(board, 0);
  mvwprintw(board, LINES / 3 + 5, 51, "P : Piege");
  wattrset(board, COLOR_PAIR(5) | A_BOLD);
  mvwprintw(board, LINES / 3 + 5, 51, "P");
  wattrset(board, 0);
  mvwprintw(board, LINES / 3 + 4, 2, "D/F : Debut / Fin ");
  wattrset(board, COLOR_PAIR(7) | A_BOLD);
  mvwprintw(board, LINES / 3 + 4, 2, "D/F");
  wattrset(board, 0);
  wrefresh(board);
}

int tailleChaine(const char *c)
{
  int i = 0;
  while (c[i] != '\0')
  {
    i++;
  }
  return i - 1;
}

int tailleInt(int a)
{
  int i = 0;
  while (a / 10 != 0)
  {
    i++;
    a = a / 10;
  }
  return i;
}

void clearAffichageChat()
{
  werase(chat);
  box(chat, ACS_DIAMOND, ACS_HLINE);
  wrefresh(chat);
  wmove(chat, chat->_cury + 1, 3);
}

void checkAffichageChat()
{
  if (chat->_cury >= chat->_maxy - 1)
  {
    clearAffichageChat(chat);
  }
}

void printChat(const char *string, ...)
{
  int cury, curx, i = 0, j = 0, a;
  va_list va;
  char *c;
  cury = getcury(chat);
  curx = getcurx(chat);
  va_start(va, string);
  while (string[i] != '\0')
  {
    if (string[i] == '%')
    {
      i++;
      switch (string[i])
      {
      case 's': /* chaîne */
        c = va_arg(va, char *);
        mvwprintw(chat, cury, curx + j, "%s", c);
        curx += tailleChaine(c);
        break;
      case 'd': /* entier */
        a = va_arg(va, int);
        mvwprintw(chat, cury, curx + j, "%d", a);
        curx += tailleInt(a);
        break;
      case 'c':
        mvwprintw(chat, cury, curx + j, "%c", (char)va_arg(va, int));
        break;
      case 'f':
        mvwprintw(chat, cury, curx + j, "%f", va_arg(va, double));
        break;
      }
    }
    else if (string[i] == '\n')
    {
      cury++;
      curx = 3;
      wmove(chat, cury, curx);
    }
    else
    {
      mvwprintw(chat, cury, curx + j, "%c", string[i]);
    }
    j++;
    i++;
  }
  va_end(va);
  wrefresh(chat);
  checkAffichageChat();
}

int scanChat(const char *string, ...)
{
  curs_set(1);
  int i = 0, g = 0;
  va_list va;
  va_start(va, string);
  while (string[i] != '\0')
  {
    if (string[i] == '%')
    {
      i++;
      switch (string[i])
      {
      case 's': /* chaîne */
        g += wscanw(chat, " %s", va_arg(va, char *));
        break;
      case 'd': /* entier */
        g += wscanw(chat, " %d", va_arg(va, int *));
        break;
      case 'c':
        g += wscanw(chat, " %c", va_arg(va, char *));
        break;
      case 'f':
        g += wscanw(chat, " %f", va_arg(va, float *));
      }
    }
    i++;
  }
  va_end(va);
  wmove(chat, getcury(chat), 3);
  checkAffichageChat();
  return g;
}

//definition de toutes les couleurs
void def_couleurs()
{
  board = subwin(stdscr, LINES, COLS, 0, 0);
  wrefresh(board);
  start_color();
  init_pair(1, COLOR_CYAN, 0);    //Couleur Bleue des cases bleues
  init_pair(2, COLOR_RED, 0);     //Couleur Rouge des cases rouges
  init_pair(3, COLOR_GREEN, 0);   // Idem pour cases evenements
  init_pair(4, COLOR_YELLOW, 0);  // Idem pour les cases magasins
  init_pair(5, COLOR_MAGENTA, 0); //Idem pour les cases pieges
  init_pair(6, COLOR_BLUE, 0);
  init_pair(7, COLOR_WHITE, 0); //Cases depart et arrivee
}

//affiche UNE case de la bonne couleur
void miniCase(char c, int x, int y)
{
  mvwprintw(board, x - 1, y - 1, ".-.");
  mvwprintw(board, x, y - 1, "| |");
  switch (c)
  {
  case 'D':
    wattrset(board, COLOR_PAIR(7) | A_BOLD);
    break;
  case 'F':
    wattrset(board, COLOR_PAIR(7) | A_BOLD);
    break;
  case '1':
    wattrset(board, COLOR_PAIR(1) | A_BOLD);
    break;
  case '0':
    wattrset(board, COLOR_PAIR(2) | A_BOLD);
    break;
  case 'X':
    wattrset(board, COLOR_PAIR(1) | A_BOLD);
    break;
  case 'M':
    wattrset(board, COLOR_PAIR(4) | A_BOLD);
    break;
  case 'P':
    wattrset(board, COLOR_PAIR(5) | A_BOLD);
    break;
  case 'V':
    wattrset(board, COLOR_PAIR(3) | A_BOLD);
    break;
  case '+':
    wattrset(board, COLOR_PAIR(7) | A_BOLD);
    break;
  }
  mvwprintw(board, x, y, "%c", c);
  wattrset(board, 0);
  mvwprintw(board, x + 1, y - 1, "'-'");
  wrefresh(board);
}

void caseTP(int x, int y)
{
  mvwprintw(board, x - 1, y - 1, ".-.");
  mvwprintw(board, x, y - 1, "| |");
  wattrset(board, COLOR_PAIR(4) | A_DIM);
  mvwprintw(board, x, y, "#");
  wattrset(board, 0);
  mvwprintw(board, x + 1, y - 1, "'-'");
  wrefresh(board);
}

void printBoard(Plateau *p, int x, int y)
{
  int compteur1 = 0;
  int compteur2 = 0;
  int etage = 0;
  int c1, c2;
  int i = 0;
  while (p->cases[i].type != fin)
  {
    switch (p->cases[i].type)
    {
    case debut:
      miniCase('D', x, y);
      y += 2;
      break;
    case gain:
      miniCase('1', x, y);
      y += 2;
      break;
    case perte:
      miniCase('0', x, y);
      y += 2;
      break;
    case croisement:
      break;
    case shop:
      miniCase('M', x, y);
      y += 2;
      break;
    case trap:
      miniCase('P', x, y);
      y += 2;
      break;
    case event:
      miniCase('V', x, y);
      y += 2;
      break;
    case nord:
      etage = 0;
      compteur1 = i;
      compteur2 = i;
      c1 = 0;
      c2 = 0;
      mvwprintw(board, x - 2, y - 2, "N");
      while (p->cases[compteur1].type != croisement)
      {
        compteur1++;
        c1++;
        if (p->cases[compteur1].type == est)
          c1 = 0;
      }
      while (p->cases[compteur2].type != est)
      {
        compteur2++;
        c2++;
        if (p->cases[compteur2].type == nord)
          c2 = 0;
      }
      int x2 = x - 2;
      int y2 = y;
      int i2 = i;
      ////
      if (p->cases[i2 + 2].type == est || p->cases[i2 + 2].type == sud)
      {
        miniCase(enumToChar(p->cases[i2 + 1].type), x2, y2);
        caseTP(x2 + 2, y2);
        break;
      }
      ////
      if ((p->cases[i2 + 1].type != est && p->cases[i2 + 2].type != est) || (p->cases[i2 + 1].type != sud && p->cases[i2 + 2].type != sud))
      {
        if ((c2 - c1) == 4 || (c2 - c1) % 2 == 1 || (c2 - c1) <= 0 || ((c2 - c1) % 2 == 0 && (c2 - c1) != 4 && (c2 - c1) != 6))
        {
          miniCase(enumToChar(p->cases[i2 + 1].type), x2, y2);
          i2 += 2;
          etage++;
        } //gestion de la taille des separations ici
        if ((c2 - c1) == 6)
        {
          miniCase(enumToChar(p->cases[i2 + 1].type), x2, y2);
          miniCase(enumToChar(p->cases[i2 + 2].type), x2 - 2, y2);
          miniCase(enumToChar(p->cases[i2 + 3].type), x2 - 4, y2 + 1);
          i2 += 3;
          etage += 2;
        }
      }

      if (c2 - c1 == 4)
      {
        while (p->cases[i2 + 1].type != est && p->cases[i2 + 1].type != sud)
        {
          miniCase(enumToChar(p->cases[i2].type), x2 - 2 * etage, y2);
          i2++;
          y2 += 2;
        }
        miniCase(enumToChar(p->cases[i2].type), x2, y2 - 2);
      }
      if (c2 - c1 == 6)
      {
        while (p->cases[i2 + 1].type != est && p->cases[i2 + 1].type != sud && p->cases[i2 + 2].type != est && p->cases[i2 + 2].type != sud)
        {
          miniCase(enumToChar(p->cases[i2].type), x2 - 2 * etage, y2);
          i2++;
          y2 += 2;
        }
        miniCase(enumToChar(p->cases[i2].type), x2 - 2, y2 - 2);
        miniCase(enumToChar(p->cases[i2 + 1].type), x2, y2 - 2);
        i2 += 2;
      }
      if ((c2 - c1) % 2 == 1 || (c2 - c1) <= 0 || ((c2 - c1) % 2 == 0 && (c2 - c1) != 4 && (c2 - c1) != 6))
      {
        while (p->cases[i2 + 1].type != est && p->cases[i2 + 1].type != sud)
        {
          miniCase(enumToChar(p->cases[i2].type), x2 - 2 * etage, y2);
          i2++;
          y2 += 2;
        }
        miniCase(enumToChar(p->cases[i2].type), x2 - 2 * etage, y2);
        y2 += 2;
        caseTP(x2 - 2 * etage, y2);
      }
      i = i2;
      etage = 0;
      break;
    case sud:
      etage = 0;
      compteur1 = i;
      compteur2 = i;
      c1 = 0;
      c2 = 0;
      mvwprintw(board, x + 2, y - 2, "S");
      while (p->cases[compteur1].type != croisement)
      {
        compteur1++;
        c1++;
        if (p->cases[compteur1].type == est)
          c1 = 0;
      }
      while (p->cases[compteur2].type != est)
      {
        compteur2++;
        c2++;
        if (p->cases[compteur2].type == sud)
          c2 = 0;
      }
      int x3 = x + 2;
      int y3 = y;
      int i3 = i;
      ////
      if (p->cases[i3 + 2].type == est)
      {
        miniCase(enumToChar(p->cases[i3 + 1].type), x3, y3);
        caseTP(x3 + 2, y3);
        break;
      }
      ////
      if ((p->cases[i3 + 1].type != est && p->cases[i3 + 2].type != est) || (p->cases[i3 + 1].type != sud && p->cases[i3 + 2].type != sud))
      {
        if ((c2 - c1) == 4 || (c2 - c1) % 2 == 1 || (c2 - c1) <= 0 || ((c2 - c1) % 2 == 0 && (c2 - c1) != 4 && (c2 - c1) != 6))
        {
          miniCase(enumToChar(p->cases[i3 + 1].type), x3, y3);
          i3 += 2;
          etage++;
        } //gestion de la taille des separations ici
        if ((c2 - c1) == 6)
        {
          miniCase(enumToChar(p->cases[i3 + 1].type), x3, y3);
          miniCase(enumToChar(p->cases[i3 + 2].type), x3 + 2, y3);
          miniCase(enumToChar(p->cases[i3 + 3].type), x3 + 4, y3 + 1);
          i3 += 3;
          etage += 2;
        } //3 de chaque coté
        //if((c2-c1)%8==0)//4 de chaque coté
        //if((c2-c1)%10==0)//5 de chaque coté
      }

      if (c2 - c1 == 4)
      {
        while (p->cases[i3 + 1].type != est && p->cases[i3 + 1].type != sud)
        {
          miniCase(enumToChar(p->cases[i3].type), x3 + 2 * etage, y3);
          i3++;
          y3 += 2;
        }
        miniCase(enumToChar(p->cases[i3].type), x3, y3 - 2);
      }
      if (c2 - c1 == 6)
      {
        while (p->cases[i3 + 1].type != est && p->cases[i3 + 1].type != sud && p->cases[i3 + 2].type != est && p->cases[i3 + 2].type != sud)
        {
          miniCase(enumToChar(p->cases[i3].type), x3 + 2 * etage, y3);
          i3++;
          y3 += 2;
        }
        miniCase(enumToChar(p->cases[i3].type), x3 + 2, y3 - 2);
        miniCase(enumToChar(p->cases[i3 + 1].type), x3, y3 - 2);
        i3 += 2;
      }

      if ((c2 - c1) % 2 == 1 || (c2 - c1) <= 0 || ((c2 - c1) % 2 == 0 && (c2 - c1) != 4 && (c2 - c1) != 6))
      {
        while (p->cases[i3 + 1].type != est && p->cases[i3 + 1].type != sud)
        {
          miniCase(enumToChar(p->cases[i3].type), x3 + 2 * etage, y3);
          i3++;
          y3 += 2;
        }

        miniCase(enumToChar(p->cases[i3].type), x3 + 2 * etage, y3);
        y3 += 2;
        caseTP(x3 + 2 * etage, y3);
      }
      i = i3;
      etage = 0;

      break;
    case est:
      miniCase('+', x, y);
      y += 2;
      break;
    ///////////////////////////////////////////////////////////////////////
    default:
      break;
    }
    i++;
  }
  miniCase('F', x, y);
}

void afficherSauvegarde(int current)
{
  WINDOW *w;
  int nb, i, j = 0, cury, curx, key;
  Plateau *p;
  initscr();
  noecho();
  w = subwin(stdscr, LINES / 4, COLS / 2, LINES / 2 - LINES / 8, COLS / 2 - COLS / 4);
  p = charger(&nb);
  if (p == NULL)
  {
    mvprintw(LINES / 2, COLS / 2 - 13, "Pas de parties sauvegardes");
    refresh();
    getch();
  }
  else
  {
    if (current == 1)
      j = nb - 1;
    curs_set(0);
    keypad(stdscr, TRUE);
    do
    {
      wclear(w);
      box(w, ACS_VLINE, ACS_HLINE);
      cury = getcury(w) + 1;
      curx = getcurx(w) + 3;
      mvwprintw(w, cury, getmaxx(w) / 2 - 10, "Partie numero %d : ", j + 1);
      if (j > 0)
      {
        mvwprintw(w, getmaxy(w) / 2, curx, "<==");
      }
      if (j < nb - 1)
      {
        mvwprintw(w, getmaxy(w) / 2, getmaxx(w) - 4, "==>");
      }
      cury += 2;
      switch (p[j].difficulty)
      {
      case 1:
        mvwprintw(w, cury, getmaxx(w) / 2 - 16, "Difficulte de la partie : Facile");
        break;
      case 2:
        mvwprintw(w, cury, getmaxx(w) / 2 - 16, "Difficulte de la partie : Moyen");
        break;
      case 3:
        mvwprintw(w, cury, getmaxx(w) / 2 - 16, "Difficulte de la partie : Difficile");
        break;
      }
      cury += 2;
      mvwprintw(w, cury, getmaxx(w) / 2 - 5, "Classement");
      cury += 2;
      for (i = 0; i < 4; i++)
      {
        mvwprintw(w, cury, getmaxx(w) / 2 - 18, " %d : %s avec %d etoiles et %d pieces", i + 1, p[j].joueurs[i].nom, p[j].joueurs[i].etoiles, p[j].joueurs[i].argent);
        cury++;
      }
      wrefresh(w);
      key = getch();
      switch (key)
      {
      case KEY_RIGHT:
        if (j < nb - 1)
          j++;
        break;
      case KEY_LEFT:
        if (j > 0)
          j--;
        break;
      default:
        break;
      }
      wmove(chat, 1, 1);
    } while (key != 27 && key != 10);
  }
  clear();
  keypad(stdscr, FALSE);
  curs_set(1);
  refresh();
  endwin();
  if (p != NULL)
  {
    for (i = 0; i < nb; i++)
    {
      supprimerPlateau(&p[i]);
    }
  }
  free(p);
}

void choixModeDeJeux(Plateau *p)
{
  int arraylength = 4, width = 15, menulength = 4;
  const char *testarray[] = {"< 1 Joueur  | 3 Robots >", "< 2 Joueurs | 2 Robots >", "< 3 Joueurs | 1 Robot  >", "     < 4 Joueurs >      "};
  initscr();
  echo();
  box(stdscr, 0, 0);

  keypad(stdscr, TRUE);
  mvprintw(1, COLS / 2 - 11, "----------------------");
  mvprintw(2, COLS / 2 - 11, "CHOIX DU MODE DE JEU :");
  mvprintw(3, COLS / 2 - 11, "----------------------");

  p->nbJoueurs = barmenu(testarray, 4, COLS / 2 - 12, arraylength, width, menulength, 0) + 1;
  p->joueurs = creerJoueurs(p->nbJoueurs, p->cases);
  curs_set(1);
  switch (p->nbJoueurs)
  {
  case 1:
    mvprintw(12, COLS / 2 - 15, "Entrez votre nom ! Joueur 1 : ");
    scanw("%s", p->joueurs[0].nom);
    p->nbJoueurs = 1;
    //creer 3 bots
    break;
  case 2:
    mvprintw(12, COLS / 2 - 15, "Entrez votre nom ! Joueur 1 : ");
    scanw("%s", p->joueurs[0].nom);
    mvprintw(14, COLS / 2 - 15, "Entrez votre nom ! Joueur 2 : ");
    scanw("%s", p->joueurs[1].nom);
    p->nbJoueurs = 2;
    //creer 2 bots
    break;
  case 3:
    mvprintw(12, COLS / 2 - 15, "Entrez votre nom ! Joueur 1 : ");
    scanw("%s", p->joueurs[0].nom);
    mvprintw(14, COLS / 2 - 15, "Entrez votre nom ! Joueur 2 : ");
    scanw("%s", p->joueurs[1].nom);
    mvprintw(16, COLS / 2 - 15, "Entrez votre nom ! Joueur 3 : ");
    scanw("%s", p->joueurs[2].nom);
    p->nbJoueurs = 3;
    //creer 1 bot
    break;
  case 4:
    mvprintw(12, COLS / 2 - 15, "Entrez votre nom ! Joueur 1 : ");
    scanw("%s", p->joueurs[0].nom);
    mvprintw(14, COLS / 2 - 15, "Entrez votre nom ! Joueur 2 : ");
    scanw("%s", p->joueurs[1].nom);
    mvprintw(16, COLS / 2 - 15, "Entrez votre nom ! Joueur 3 : ");
    scanw("%s", p->joueurs[2].nom);
    mvprintw(18, COLS / 2 - 15, "Entrez votre nom ! Joueur 4 : ");
    scanw("%s", p->joueurs[3].nom);
    p->nbJoueurs = 4;
    break;
  }
  curs_set(0);
  clear();
  refresh();
  endwin();
}

int afficherBoardPic(int selection)
{
  Plateau temp;
  char file[50];
  selection++;
  sprintf(file, "map/map%d.txt", selection);
  wclear(board);
  box(stdscr, 0, 0);
  wrefresh(board);
  mvprintw(1, COLS / 2 - 11, "----------------------");
  mvprintw(2, COLS / 2 - 8, "CHOIX DU NIVEAU :");
  mvprintw(3, COLS / 2 - 11, "----------------------");
  mvprintw(2, COLS / 3 - 11, ". o   ");
  mvprintw(3, COLS / 3 - 11, "`'#v--");
  mvprintw(4, COLS / 3 - 11, " /'>  ");
  mvprintw(8, COLS / 7, ". o   ");
  mvprintw(9, COLS / 7, "`'#v--");
  mvprintw(10, COLS / 7, " /'>  ");
  mvprintw(5, COLS / 10, " 0   ");
  mvprintw(6, COLS / 10, "/0---");
  mvprintw(7, COLS / 10, "/ >  ");
  ////////////////////////////////
  mvprintw(2, 2 * COLS / 3, "   c ,");
  mvprintw(3, 2 * COLS / 3, "--v#`'");
  mvprintw(4, 2 * COLS / 3, "  <`\\ ");
  mvprintw(8, 5 * COLS / 7, "\\0");
  mvprintw(9, 5 * COLS / 7, " :\\ ");
  mvprintw(10, 5 * COLS / 7, "/ >");
  mvprintw(5, 8 * COLS / 10, "   c ,");
  mvprintw(6, 8 * COLS / 10, "--v#`'");
  mvprintw(7, 8 * COLS / 10, "  <`\\ ");
  refresh();

  temp = importBoard(file);
  if (temp.nbCases != -1)
  {
    printBoard(&temp, LINES / 2 + 3, COLS / 2 - temp.nbCases / 2 - temp.nbCases / 4 - 5);
    refresh();
    return 1;
  }
  else
  {
    mvprintw(LINES / 2, COLS / 2 - 14, "Ce plateau n'est pas correct");
  }
  return 0;
}

FILE *getMapFile(int val)
{
  FILE *f;
  char file[50];
  sprintf(file, "map/map%d.txt", val);
  if ((f = fopen(file, "r")) != NULL)
  {
    fclose(f);
    return f;
  }
  return NULL;
}

int selectionPlateau()
{
  FILE *f;
  int selection, arraylength = 1, width = 9, menulength = 5;
  char testarray[256][50];
  while ((f = getMapFile(arraylength)) != NULL)
  {
    sprintf(testarray[arraylength - 1], "< MAP %d >", arraylength);
    arraylength++;
  }

  if (arraylength == 1)
  {
    return -1;
  }
  initscr();
  echo();
  box(stdscr, 0, 0);
  keypad(stdscr, TRUE);
  mvprintw(1, COLS / 2 - 11, "----------------------");
  mvprintw(2, COLS / 2 - 8, " CHOIX DU NIVEAU :");
  mvprintw(3, COLS / 2 - 11, "----------------------");

  selection = barmenuBis(testarray, 5, COLS / 2 - 5, arraylength - 1, width, menulength, 0) + 1;
  erase();
  refresh();
  endwin();

  return selection;
}

void printCharByChar(int x, int y, char *chaine)
{
  int i;
  for (i = 0; chaine[i] != '\0'; i++)
  {
    mvprintw(x, y++, "%c", chaine[i]);
    refresh();
    napms(50);
  }
}

void tutoriel()
{
  initscr();
  keypad(stdscr, TRUE);
  noecho();
  def_couleurs();
  refresh();

  attrset(0);

  box(stdscr, 0, 0);
  refresh();
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(10, 18, "Salut ! Moi c'est K1w1 !");
  printCharByChar(11, 10, "Bienvenue dans Awesome Party 1 ! Je suis là pour te présenter les régles de ce jeu !");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(10, 15, "Tout d'abord, chaque partie se déroule au tour à tour.");
  printCharByChar(11, 20, "Les joueurs doivent lancer un dé pour se déplacer sur le plateau");
  printCharByChar(12, 25, "Les effets des cases sur lesquels ils s'arrêtent diffèrent dans chaque jeu.");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(10, 15, "Laisse moi te présenter les différentes cases du plateau !");
  printCharByChar(11, 20, "D'abord la case gain. La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|1|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "Si tu t'arrete a cette case a la fin de ton tour tu gagneras des pieces !");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "Maintenant la case perte. La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|0|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "Si tu t'arrete a cette case a la fin de ton tour tu perdras des pieces !");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "Maintenant la case du magasin . La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|M|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "Si tu t'arrete a cette case a la fin de ton tour tu pourras acheter des objets utiles, je t'en parle juste apres !");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "Maintenant la case du debut du plateau. La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|D|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "Lorsque tu lances une partie, tu demarres a la case debut !");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "Maintenant la case de fin du plateau. La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|F|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "C'est la derniere case du plateau, lorsque tu y arrives tu te retrouves teleporte au debut!");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "Maintenant la case des pieges ! La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|P|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "Cette case indique qu'un piege a été posé ! Fais y attention ! ");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "Maintenant la case des evenements. La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|V|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "Lorsque tu termines ton tour sur cette case un evenement aleatoire se lance !");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "Maintenant la case des points cardinaux, voici la premiere, la case nord. La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|N|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "Cette case indique la présence d'une direction nord dans le plateau !");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "La case sud. La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|S|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "Cette case indique la présence d'une direction sud dans le plateau !");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "Et la case est. La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|E|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "Cette case indique la présence d'une direction est dans le plateau !");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "Et enfin la case croisement La voici :");
  printCharByChar(12, 25, ".-.");
  printCharByChar(13, 25, "|X|");
  printCharByChar(14, 25, "._.");
  printCharByChar(15, 25, "Cette case indique ndique juste la presence d'un croisement sur le plateau !");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(11, 20, "Il y a un truc incroyable je ne t'ai pas encre dit...");
  printCharByChar(12, 25, "Il t'est possible de creer et de modifier tes propres plateaux !");
  printCharByChar(13, 25, "Mais il y a deux trois regles a respecter");
  printCharByChar(14, 25, "Un plateau commence par le caractére « D » fini par le caractere « F ».");
  printCharByChar(15, 25, "Le plateau doit se trouver dans le dossier « map ».");
  printCharByChar(16, 25, "Le nombre de cases du plateau doit figurer au début du fichier.");
  printCharByChar(17, 25, "Lors de la création de branche les points cardinaux doivent être placé dans cette ordre : Nord, Sud, Est.");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(10, 15, "Enfin il t'es possible d'acheter des objet dans le petit magasin !");
  printCharByChar(11, 20, "Des objets comme des piege ou des des double ou triple ! ");
  printCharByChar(12, 20, "Bien sur ils te couteront des pieces.");
  refresh();
  getch();
  erase();
  box(stdscr, 0, 0);
  mvprintw(LINES / 2 - 9, COLS / 2 - 10, "/\\^.w.^/\\ ");
  printCharByChar(10, 15, "Voila je t'ai explique le fonctionnement du jeu.");
  printCharByChar(11, 20, "Je te suhaite une bonne partie !");
  refresh();
  getch();
  erase();
}
