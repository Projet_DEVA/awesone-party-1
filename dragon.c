#include "dragon.h"

int *dragon(int nbJoueurs, char **joueurs, int difficulty)
{
    int *p = (int*)calloc(4, sizeof(int));
    int temp, k = -1, val = 0;
    int nb;
    switch(difficulty){
        case 1:
            nb = (rand() % 15) + 15;
        break;
        case 2:
            nb = (rand() % 15) + 10;
        break;
        case 3:
            nb = (rand() % 15) + 5;
        break;
    }
    do
    {
        k++;
        if (k == 4)
            k = 0;
        printChat("Joueur %s, c'est a votre tour !\n", joueurs[k]);
        if (k < nbJoueurs)
        {
            do
            {
                printChat("Allez-vous prendre une, deux ou trois pieces au dragon ? : ");
                scanChat("%d", &temp);
            } while (temp < 1 || temp > 3);
        }
        else
        {
            printChat("Allez-vous prendre une, deux ou trois pieces au dragon ? : %d\n", (temp = rand()%3 + 1));
            napms(2000);
        }
        clearAffichageChat();
        p[k] += temp;
        val += temp;
    } while (val < nb);
    printChat("Le joueur %s a reveiller le dragon !", joueurs[k]);
    p[k] = 0;
    return p;
}