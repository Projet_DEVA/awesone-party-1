#ifndef plusmoins_h
#define plusmoins_h
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

int *plusMoins(int nbJoueurs, char** joueurs, int difficulty);
extern void printChat(const char *string, ...);
extern int scanChat(const char *string, ...);
extern void clearAffichageChat();

#endif // PLUSMOINS_H_INCLUDED
