#include "shop.h"

void affecterPrix(Objet *O)
{
  switch (O->id)
  {
  case teleporteur:
    O->prix = 20;
    break;
  case vol:
    O->prix = 15;
    break;
  case recule:
    O->prix = 10;
    break;
  case piege:
    O->prix = 25;
    break;
  case loose:
    O->prix = 15;
    break;
  case de2:
    O->prix = 20;
    break;
  case de3:
    O->prix = 30;
    break;
  default:
    break;
  }
}

void affecterNom(Objet *O)
{
  switch (O->id)
  {
  case teleporteur:
    O->nomObjet = (char *)"Teleporteur"; // Teleporte a un autre joueur
    break;
  case vol:
    O->nomObjet = (char *)"Vol d'argent"; // Vol de l'argent a un autre joueur
    break;
  case recule:
    O->nomObjet = (char *)"Recule"; // Fait reculer un joueur
    break;
  case piege:
    O->nomObjet = (char *)"Piege"; // Fait perdre de l'argent a un joueur
    break;
  case loose:
    O->nomObjet = (char *)"Perte";
    break;
  case de2:
    O->nomObjet = (char *)"De Double"; // Lance un des supplementaire
    break;
  case de3:
    O->nomObjet = (char *)"De Triple"; // Lance 2 des supplementaire
    break;
  default:
    break;
  }
}

Objet genererObjet(enum idObjet id)
{
  Objet o;
  o.id = id;
  affecterNom(&o);
  affecterPrix(&o);
  return o;
}

void supprimerObjet(Objet *o)
{
  o->id = nothing;
  o->prix = 0;
}
