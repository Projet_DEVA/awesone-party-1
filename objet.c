#include "objet.h"

int teleport(Plateau *p, Joueur *j)
{
  int num;
  if (j->bot == 0)
  {
    do
    {
      printChat("A quel joueur voulez-vous vous teleporter ? :(1-4) ");
      scanChat("%d", &num);
    } while ((p->joueurs[num - 1].numero == j->numero || num < 1 || num > 4 )&& num != -1);
  }
  else
  {
    do
    {
      num = rand() % 4 + 1;
    } while (num == j->numero);
  }
  if (num != -1)
  {
    j->position = p->joueurs[num - 1].position;
  }
  else{
    return 0;
  }
  return 1;
}

int voler(Plateau *p, Joueur *j)
{
  int num;
  if (j->bot == 0)
  {
    do
    {
      printChat("Quel joueur voulez-vous piller ? :(1-4) ");
      scanChat("%d", &num);
    } while ((p->joueurs[num - 1].numero == j->numero || num < 1 || num > 4 )&& num != -1);
  }
  else
  {
    do
    {
      num = rand() % 4 + 1;
    } while (num == j->numero);
  }
  if (num != -1)
  {
    p->joueurs[num - 1].argent -= 5;
    j->argent += 5;
    if (p->joueurs[num - 1].argent < 0)
    {
      j->argent += p->joueurs[num - 1].argent;
      p->joueurs[num - 1].argent = 0;
    }
  }
  else{
    return 0;
  }
  return 1;
}

int reculer(Plateau *p, Joueur *j)
{
  int num;
  if (j->bot == 0)
  {
    do
    {
      printChat("Quel joueur voulez-vous faire reculer ? :(1-4) ");
      scanChat("%d", &num);
    } while ((p->joueurs[num - 1].numero == j->numero || num < 1 || num > 4 )&& num != -1);
  }
  else
  {
    do
    {
      num = rand() % 4 + 1;
    } while (num == j->numero);
  }
  if (num != -1)
  {
    if (p->joueurs[num - 1].position->indiceTab - 5 < 0)
    {
      p->joueurs[num - 1].position = &p->cases[0];
    }
    else
    {
      p->joueurs[num - 1].position = &p->cases[p->joueurs[num - 1].position->indiceTab - 5];
    }
  }
  else{
    return 0;
  }
  return 1;
}

void poserPiege(Joueur *j)
{
  changerTypeCase(j->position, trap);
}

int perteObjet(Plateau *p, Joueur *j)
{
  int num, val, k;
  if (j->bot == 0)
  {
    do
    {
      printChat("Pour quel joueur voulez-vous faire disparaitre un objet ? :(1-4) ");
      scanChat("%d", &num);
    } while ((p->joueurs[num - 1].numero == j->numero || p->joueurs[num - 1].nbObjet == 0 || num < 1 || num > 4 )&& num != -1);
  }
  else
  {
    do
    {
      num = rand() % 4 + 1;
    } while (num == j->numero);
  }
  if (num != -1)
  {
    if(p->joueurs[num - 1].nbObjet != 0){
      val = rand() % p->joueurs[num - 1].nbObjet;
      k = enleverObjet(&p->joueurs[num - 1], val);
    }
    else k = 0;
  }
  else{
    return 0;
  }
  return k;
}
