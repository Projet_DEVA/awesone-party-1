#include <stdio.h>
#include <stdlib.h>
#include "case.h"

Case creerCase(int indice, enum typecase type)
{
  Case cas;
  cas.indiceTab = indice;
  cas.type = type;
  return cas;
}

Case creerCaseRandom(int indice)
{
  enum typecase randomChoice = (enum typecase)(rand() % 5);
  return creerCase(indice, randomChoice);
}

void changerTypeCase(Case *cas, enum typecase type)
{
  cas->type = type;
}
