#include "spamMe.h"

int *spamMe(int nbJoueurs, char **joueurs, int difficulty)
{
  clearAffichageChat();
  int *scorebuffer = (int *)calloc(nbJoueurs, sizeof(int));    // Garde en memoire le premier input de touche du joueur
  char *lettre = (char *)malloc(sizeof(char) * nbJoueurs * 2); // Garde en memoire les touches que devront utiliser les joueurs
  int i;
  int *score = (int *)calloc(4, sizeof(int)); // Garde en memoire le score des joueurs
  for (i = 0; i < nbJoueurs * 2; i++)
  {
    lettre[i] = 'a' + rand() % 26;
    if (i == 1 && lettre[i] == lettre[i - 1])
      i--;
    else if (i == 2 && (lettre[i] == lettre[i - 1] || lettre[i] == lettre[i - 2]))
      i--;
    else if (i == 3 && (lettre[i] == lettre[i - 1] || lettre[i] == lettre[i - 2] || lettre[i] == lettre[i - 3]))
      i--;
    else if (i == 4 && (lettre[i] == lettre[i - 1] || lettre[i] == lettre[i - 2] || lettre[i] == lettre[i - 3] || lettre[i] == lettre[i - 4]))
      i--;
    else if (i == 5 && (lettre[i] == lettre[i - 1] || lettre[i] == lettre[i - 2] || lettre[i] == lettre[i - 3] || lettre[i] == lettre[i - 4] || lettre[i] == lettre[i - 5]))
      i--;
    else if (i == 6 && (lettre[i] == lettre[i - 1] || lettre[i] == lettre[i - 2] || lettre[i] == lettre[i - 3] || lettre[i] == lettre[i - 4] || lettre[i] == lettre[i - 5] || lettre[i] == lettre[i - 6]))
      i--;
    else if (i == 7 && (lettre[i] == lettre[i - 1] || lettre[i] == lettre[i - 2] || lettre[i] == lettre[i - 3] || lettre[i] == lettre[i - 4] || lettre[i] == lettre[i - 5] || lettre[i] == lettre[i - 6] || lettre[i] == lettre[i - 7]))
      i--;
  } // Attribut deux lettres aleatoire aux joueurs
  for (i = 0; i < nbJoueurs; i++)
  {
    printChat("Joueur %s : %c <--> %c\n", joueurs[i], lettre[i], lettre[i + nbJoueurs]);
  }
  noecho();
  time_t a = time(NULL) + 10; // Permet de faire durer la partie pendant environ 10 secondes
  char ch;
  while (time(NULL) < a)
  {
    ch = getch(); // recupere le caractere saisie par un joueur
    for (i = 0; i < nbJoueurs; i++)
    {
      if (ch == lettre[i] && scorebuffer[i] == 0)
      {
        scorebuffer[i]++;
      }
      else if (ch == lettre[i + nbJoueurs] && scorebuffer[i] == 1)
      {
        score[i]++;
        scorebuffer[i]--;
      }
    } // Pour chaque joueur, verifie quel caractere a ete saisie
  }
  for (i = 0; i < 4 - nbJoueurs; i++)
  {
    switch (difficulty)
    {
    case 1:
      score[i + nbJoueurs] = rand() % (25 + 1 - 15) + 15;
      break;
    case 2:
      score[i + nbJoueurs] = rand() % (35 + 1 - 25) + 25;
      break;
    case 3:
      score[i + nbJoueurs] = rand() % (45 + 1 - 35) + 35;
      break;
    }
  } // Choisi un score aleatoire pour chaque bot
  clearAffichageChat();
  echo();
  free(scorebuffer); // Liberation de la memoire
  free(lettre);

  return score; // Retour du score
}
