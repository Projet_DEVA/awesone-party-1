#ifndef shop_h
#define shop_h
#include <time.h>
#include <stdlib.h>

enum idObjet
{
  nothing,
  teleporteur,
  vol,
  recule,
  piege,
  loose,
  de2,
  de3
};

typedef struct
{
  char *nomObjet;
  enum idObjet id;
  int prix;
} Objet;

Objet *choixObjet(Objet *O1, Objet *O2, Objet *O3);
void supprimerObjet(Objet *o);
Objet genererObjet(enum idObjet id);

#endif
