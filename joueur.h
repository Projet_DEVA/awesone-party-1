#ifndef joueur_h
#define joueur_h
#include "case.h"
#include "shop.h"
#include "miniJeu.h"
#include <string.h>

typedef struct
{
    char *nom;
    int numero;
    int nbObjet;
    Objet *inventaire;
    int argent;
    int etoiles;
    Case *position;
    int bot;
} Joueur;

Joueur *creerJoueurs(int nb, Case *caseDepart);

void supprimerJoueur(Joueur *joueurs, int nbjoueurs);

void miniJeux(Joueur *joueurs, int nbJoueurs, int difficulty);

void plus(Joueur *joueur, int nbArgent);

void moins(Joueur *joueur, int nbArgent);

void magasin(Joueur *joueur);

int enleverObjet(Joueur *joueur, int index);

void prendrePiege(Joueur *joueur, int nbArgent);

void demanderEtoile(Joueur *joueur, int difficulty);

extern void checkAffichageChat();
extern void clearAffichageChat();
extern void printChat(const char *string, ...);
extern int scanChat(const char *string, ...);
extern int lancerDesJoueur();
extern int barmenu(const char **array, const int row, const int col, const int arraylength, const int width, int menulength, int selection);

#endif
