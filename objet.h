#ifndef objet_h
#define objet_h
#include "plateau.h"

int teleport(Plateau *p, Joueur *j);

int voler(Plateau *p, Joueur *j);

int reculer(Plateau *p, Joueur *j);

void poserPiege(Joueur *j);

int perteObjet(Plateau *p, Joueur *j);

#endif
