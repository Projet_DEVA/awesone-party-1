#include "event.h"

void eventAjoutnbArgent(int *nbArgentPlus, int difficulty)
{
  if (difficulty == 1)
    *nbArgentPlus += 3;
  if (difficulty == 2)
    *nbArgentPlus += 2;
  if (difficulty == 3)
    *nbArgentPlus += 1;
  printChat("Les cases qui ajoutent de l'argent ajoutent desormais %d pieces!\n", *nbArgentPlus);
}

void eventRetraitnbArgent(int *nbArgentMoins, int difficulty)
{

  if (difficulty == 1)
    *nbArgentMoins += 1;
  if (difficulty == 2)
    *nbArgentMoins += 2;
  if (difficulty == 3)
    *nbArgentMoins += 3; //soit gain de 0 par case gain si non changement precedent
  printChat("Les cases qui retirent de l'argent retirent desormais %d pieces!\n", *nbArgentMoins);
}

void eventAjoutArgent(Plateau *p, int difficulty)
{
  int i, money;
  printChat("Oh, un evenement qui ajoute de l'argent a tout le monde se produit !\n");
  switch(difficulty){
    case 1:
      money = 5;
      break;
    case 2:
      money = 3;
      break;
    case 3:
      money = 2;
      break;
  }
  for (i = 0; i < p->nbJoueurs; i++)
  {
    p->joueurs[i].argent += money;
    if (p->joueurs[i].argent < 0)
    {
      p->joueurs[i].argent = 0;
    }
  }
  printChat("Tout le monde viens de gagner %d pieces !\n", money);
}

void eventRetraitArgent(Plateau *p, int difficulty)
{
  int i, money;
  printChat("Oh non, un evenement qui retire de l'argent a tout le monde se produit !\n");
  switch(difficulty){
    case 1:
      money = 3;
      break;
    case 2:
      money = 5;
      break;
    case 3:
      money = 10;
      break;
  }
  for (i = 0; i < p->nbJoueurs; i++)
  {
    p->joueurs[i].argent -= money;
    if (p->joueurs[i].argent < 0)
    {
      p->joueurs[i].argent = 0;
    }
  }
  printChat("Tout le monde viens de perdre %d pieces !\n",money);
}

void eventInverserCase(Plateau *p)
{
  printChat("Un evenement qui inverse les case de gain et de perde d'argent se produit !\n");
  for (int i = 0; i < p->nbCases; i++)
  {
    if (p->cases[i].type == gain)
      changerTypeCase(&p->cases[i], perte);
    else if (p->cases[i].type == perte)
      changerTypeCase(&p->cases[i], gain);
  }
  printChat("Les cases sont maintenant changees !\n");
}

void startEvent(Plateau *p)
{
  int w = time(NULL) % 5 + 1;
  switch (w)
  {
  case 1:
    eventAjoutnbArgent(&p->nbArgentPlus, p->difficulty);
    break;
  case 2:
    eventRetraitnbArgent(&p->nbArgentMoins, p->difficulty);
    break;
  case 3:
    eventAjoutArgent(p, p->difficulty);
    break;
  case 4:
    eventRetraitArgent(p, p->difficulty);
    break;
  case 5:
    eventInverserCase(p);
    break;
  }
}
