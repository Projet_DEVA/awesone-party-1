#ifndef dragon_h
#define dragon_h
#include <stdio.h>
#include <stdlib.h>
#include <curses.h>

int *dragon(int nbJoueurs, char** joueurs, int difficulty);
extern void printChat(const char *string, ...);
extern int scanChat(const char *string, ...);
extern void clearAffichageChat();

#endif // PLUSMOINS_H_INCLUDED
