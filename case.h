#ifndef case_h
#define case_h

enum typecase
{
  gain,
  perte,
  event,
  shop,
  trap,
  debut,
  fin,
  nord,
  est,
  sud,
  croisement
};

typedef struct
{
  int indiceTab;
  enum typecase type;
} Case;

Case creerCase(int indice, enum typecase type);

Case creerCaseRandom(int indice);

void changerTypeCase(Case *cas, enum typecase type);

#endif
